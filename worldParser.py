import argparse

import os
import datetime

import MinecraftWorldParser
from MinecraftWorldParser import BlockFilter, BlockExtractor, JsonBlockWriter
from MinecraftWorldParser import postprocessing

import MinecraftElements

def run(input_path, output_path, lower_bound, upper_bound, blocksToIgnore, indent, metadata):

	# Set up a BlockExtractor to only extract blocks from the region of interest

	blockFilter = BlockFilter(lower_bound = lower_bound, upper_bound = upper_bound, blocksToIgnore = blocksToIgnore)
	blockExtractor = BlockExtractor(input_path, blockFilter)

	# Extract blocks
	blocks = blockExtractor.extract()

	# Perform post-processing
	postprocessing.processDoors(blockExtractor)

	postprocessing.processBars(blockExtractor)
	postprocessing.processGlassPanes(blockExtractor)
	postprocessing.processFences(blockExtractor)
	postprocessing.processWalls(blockExtractor)
	postprocessing.processStairs(blockExtractor)

	JsonBlockWriter(blockExtractor).dump(output_path, metadata, indent)


def getParser():
	"""
	Function for creating a parser for command line arguments, to simplify things
	"""

	parser = argparse.ArgumentParser()
	parser.add_argument("world_path", help="path to the Minecraft world")
	parser.add_argument("output_file", help="filename to write output to")
	parser.add_argument("-l", "--lower_bound", help="lower boundary of blocks to extract.  Accepts three arguments, i.e., x_min y_min z_min", nargs=3, type=int)
	parser.add_argument("-u", "--upper_bound", help="upper boundary of blocks to extract.  Accepts three arguments, i.e., x_max y_max z_max", nargs=3, type=int)
	parser.add_argument("-p", "--pretty_print", help="indicate if the output file should be pretty printed.  Each flag increments the indentation, i.e., -ppp will indent by 3 spaces.", action="count", default=0)
	parser.add_argument("-i", "--ignore", nargs="*", help="indicate a block to be ignored.  May be used multiple times.", default=['air'])
	parser.add_argument("--world_url", help="URL of the Minecraft world file", default="Not Provided")
	parser.add_argument("--map_url", help="URL of the JSON Map file", default="Not Provided")


	return parser



if __name__ == '__main__':
	# Parse command line arguments
	parser = getParser()
	args = parser.parse_args()

	# Get the required arguments (input and output paths)
	world_path = args.world_path
	output_file = args.output_file

	# Create the lower and upper bounds
	if args.lower_bound:
		lower_bound = tuple(args.lower_bound)
	else:
		lower_bound = (-1e52, 0, -1e52)

	if args.upper_bound:
		upper_bound = tuple(args.upper_bound)
	else:
		upper_bound = (1e52, 255, 1e25)

	if args.pretty_print == 0:
		args.pretty_print = None

	# Get the blocks to ignore, and make sure 'air' is present
	ignoreBlocks = args.ignore
	if not 'air' in ignoreBlocks:
		ignoreBlocks.append('air')


	metadata = { 'map_name': output_file.strip(os.path.sep).split(os.path.sep)[-1],
	             'world_name': world_path.strip(os.path.sep).split(os.path.sep)[-1],
	             'world_url': args.world_url,
	             'map_url': args.map_url,
	             'creation_time': datetime.datetime.now().isoformat()+'Z',
	             'lower_bound': { 
	             	'x': lower_bound[0],
	            	'y': lower_bound[1],
	                'z': lower_bound[2]
	             },
				 'upper_bound': { 
				 	'x': upper_bound[0],
	                'y': upper_bound[1],
	                'z': upper_bound[2]
	             },
	             'ignored_blocks': ignoreBlocks,
	             'parser_metadata': { 
	             	'name': MinecraftWorldParser.__name__,
	                'version': MinecraftWorldParser.__version__,
	                'url': MinecraftWorldParser.__url__,
	                'dependencies': [
	                	{
	                		'package': MinecraftElements.__name__,
	                		'version': MinecraftElements.__version__,
	                		'url':     MinecraftElements.__url__
	                	}
	                ]
	             }
				}

	blocksToIgnore = [ MinecraftElements.Block[block] for block in ignoreBlocks]

	run(args.world_path, args.output_file, lower_bound, upper_bound, blocksToIgnore, args.pretty_print, metadata)


