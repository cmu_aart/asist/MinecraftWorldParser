"""
A collection of classes to use to write extracted data to specific formats.
Currently consists of the following classes:

JsonBlockWriter - writes extracted data to a json file.
"""

import json

class JsonBlockWriter:
	"""
	Class to write blocks to a JSON file
	"""


	@staticmethod
	def toJson(block):
		"""
		Convert the information in the block to a json format.

		Args:
			block - instance of a Block to convert to json.

		Return:
			A dictionary containing data to write as json.  (Note that the data
			is not returned as an actual JSON string).
		"""

		jsondata = {}

		# Common things to all blocks
		jsondata['type'] = block.type.name
		jsondata['location'] = { 'x': block.location[0],
		                         'y': block.location[1],
		                         'z': block.location[2]}

		for name, value in block.properties.items():
			if name in ['facing', 'half', 'hinge', 'shape']:
				jsondata[name] = value.name
			else:
				jsondata[name] = value

		return jsondata


	def __init__(self, blockExtractor):
		"""
		Create a new JSON block writer.

		Args:
			blockExtractor - instance of a BlockExtractor to gather data from.
		"""

		self.blockExtractor = blockExtractor


	def dump(self, filename, metadata, indent=None):
		"""
		Write the information to the file

		Args:
			filename - the path to write the JSON data to
			indent   - number of spaces to indent each level.  If None  or a
					   negative number is provided, json will not be 
					   pretty-printed.
		"""

		json_data = {}

		# Get the data from all the blocks as json
		json_data['blocks'] = [JsonBlockWriter.toJson(block) for block in self.blockExtractor.blocks]
		json_data['metadata'] = metadata

		with open(filename, 'w') as jsonFile:
			json.dump(json_data, jsonFile, indent=indent)