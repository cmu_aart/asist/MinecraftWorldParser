"""
Post processing functions for augmenting information contained in blocks, or 
generating new information from extracted blocks.

Currently implemented post processing functions:

processDoors - merges the properties extracted from upper and lower door halves
"""

import MinecraftElements

## HELPER DATA ##

"""
All the types of doors in Minecraft
"""
DOOR_TYPES = set([ MinecraftElements.Block.wooden_door,
                   MinecraftElements.Block.iron_door,
                   MinecraftElements.Block.spruce_door,
                   MinecraftElements.Block.birch_door,
                   MinecraftElements.Block.jungle_door,
                   MinecraftElements.Block.acacia_door,
                   MinecraftElements.Block.dark_oak_door ])

"""
All the block types that will form connections with glass, bars, walls and fences
"""
CONNECTION_TYPES = set([ MinecraftElements.Block.stone,
						 MinecraftElements.Block.grass,
						 MinecraftElements.Block.dirt,
						 MinecraftElements.Block.cobblestone,
						 MinecraftElements.Block.planks,
						 MinecraftElements.Block.bedrock,
						 MinecraftElements.Block.sand,
						 MinecraftElements.Block.gravel,
						 MinecraftElements.Block.gold_ore,
						 MinecraftElements.Block.iron_ore,
						 MinecraftElements.Block.coal_ore,
						 MinecraftElements.Block.log,
						 MinecraftElements.Block.sponge,
						 MinecraftElements.Block.lapis_ore,
						 MinecraftElements.Block.lapis_block,
						 MinecraftElements.Block.dispenser,
						 MinecraftElements.Block.sandstone,
						 MinecraftElements.Block.noteblock,
						 MinecraftElements.Block.wool,
						 MinecraftElements.Block.gold_block,
						 MinecraftElements.Block.iron_block,
						 MinecraftElements.Block.double_stone_slab,
						 MinecraftElements.Block.brick_block,
						 MinecraftElements.Block.bookshelf,
						 MinecraftElements.Block.mossy_cobblestone,
						 MinecraftElements.Block.obsidian,
						 MinecraftElements.Block.diamond_ore,
						 MinecraftElements.Block.diamond_block,
						 MinecraftElements.Block.crafting_table,
						 MinecraftElements.Block.furnace,
						 MinecraftElements.Block.lit_furnace,
						 MinecraftElements.Block.redstone_ore,
						 MinecraftElements.Block.lit_redstone_ore,
						 MinecraftElements.Block.snow,
						 MinecraftElements.Block.clay,
						 MinecraftElements.Block.jukebox,
						 MinecraftElements.Block.netherrack,
						 MinecraftElements.Block.soul_sand,
						 MinecraftElements.Block.lit_pumpkin,
						 MinecraftElements.Block.stained_glass,
						 MinecraftElements.Block.monster_egg,
						 MinecraftElements.Block.stonebrick,
						 MinecraftElements.Block.mycelium,
						 MinecraftElements.Block.nether_brick,
						 MinecraftElements.Block.nether_wart,
						 MinecraftElements.Block.redstone_lamp,
						 MinecraftElements.Block.lit_redstone_lamp,
						 MinecraftElements.Block.double_wooden_slab,
						 MinecraftElements.Block.emerald_ore,
						 MinecraftElements.Block.emerald_block,
						 MinecraftElements.Block.redstone_block,
						 MinecraftElements.Block.quartz_ore,
						 MinecraftElements.Block.quartz_block,
						 MinecraftElements.Block.dropper,
						 MinecraftElements.Block.stained_hardened_clay,
						 MinecraftElements.Block.log2,
						 MinecraftElements.Block.slime,
						 MinecraftElements.Block.prismarine,
						 MinecraftElements.Block.hay_block,
						 MinecraftElements.Block.hardened_clay,
						 MinecraftElements.Block.coal_block,
						 MinecraftElements.Block.packed_ice,
						 MinecraftElements.Block.red_sandstone,
						 MinecraftElements.Block.double_stone_slab2,
						 MinecraftElements.Block.purpur_block,
						 MinecraftElements.Block.purpur_pillar,
						 MinecraftElements.Block.purpur_double_slab,
						 MinecraftElements.Block.end_bricks,
						 MinecraftElements.Block.magma,
						 MinecraftElements.Block.nether_wart_block,
						 MinecraftElements.Block.red_nether_brick,
						 MinecraftElements.Block.bone_block,
						 MinecraftElements.Block.observer
	])


"""
All the block types that will form connections with glass and bars, but not
walls or fences
"""
GLASS_AND_BAR_CONNECTION_TYPES = set([ MinecraftElements.Block.leaves,
									   MinecraftElements.Block.glass,
									   MinecraftElements.Block.tnt,
									   MinecraftElements.Block.ice,
									   MinecraftElements.Block.pumpkin,
									   MinecraftElements.Block.glowstone,
									   MinecraftElements.Block.iron_bars,
									   MinecraftElements.Block.glass_pane,
									   MinecraftElements.Block.melon_block,
									   MinecraftElements.Block.stained_glass_pane,
									   MinecraftElements.Block.leaves2,
									   MinecraftElements.Block.sea_lantern
	])



"""
Set of block types of each stair.  These types of blocks will connect to glass
or bars, but only if bordered by the full vertical face of the stair
"""
STAIR_TYPES = set([ MinecraftElements.Block.oak_stairs,
					MinecraftElements.Block.stone_stairs,
					MinecraftElements.Block.brick_stairs,
					MinecraftElements.Block.stone_brick_stairs,
					MinecraftElements.Block.spruce_stairs,
					MinecraftElements.Block.birch_stairs,
					MinecraftElements.Block.jungle_stairs,
					MinecraftElements.Block.nether_brick_stairs,
					MinecraftElements.Block.sandstone_stairs,
					MinecraftElements.Block.quartz_stairs,
					MinecraftElements.Block.acacia_stairs,
					MinecraftElements.Block.dark_oak_stairs,
					MinecraftElements.Block.red_sandstone_stairs,
					MinecraftElements.Block.purpur_stairs
	])


	

"""
Set of fence types.  Fences will connect to fences of the same type.  e.g., 
a spruce_fence will connect to a spruce_fence, but not a birch_fence
"""
FENCE_TYPES = set([ MinecraftElements.Block.fence,
					MinecraftElements.Block.nether_brick_fence,
					MinecraftElements.Block.spruce_fence,
					MinecraftElements.Block.birch_fence,
					MinecraftElements.Block.jungle_fence,
					MinecraftElements.Block.dark_oak_fence,
					MinecraftElements.Block.acacia_fence
	])
	

"""
Set of gate types.  These will connect to walls and fences.
"""
GATE_TYPES = set([ MinecraftElements.Block.fence_gate,
				   MinecraftElements.Block.spruce_fence_gate,
				   MinecraftElements.Block.birch_fence_gate,
				   MinecraftElements.Block.jungle_fence_gate,
				   MinecraftElements.Block.dark_oak_fence_gate,
				   MinecraftElements.Block.acacia_fence_gate
	])



# If block is at (x,y,z), then these connection flags are set if a valid block is located at:
#
# (x-1, y, z) - WEST
# (x+1, y, z) - EAST
# (x, y, z-1) - NORTH
# (x, y, z+1) - SOUTH

"""
THESE ARE MAYBE BLOCKS
	piston_head = 34 (?)
	farmland = 60 (?)
	brown_mushroom_block = 99 (?)
	red_mushroom_block = 100 (?)
	pumpkin_stem = 104 (?)
	melon_stem = 105 (?)
	piston_extension = 36 (?)
	end_stone = 121 (?)
	dragon_egg = 122 (?)
	cocoa = 127 (?)
	command_block = 137 (?)
	barrier = 166 (?)
	beetroots = 207 (?)
	grass_path = 208 (?)
	end_gateway = 209 (?)
	repeating_command_block = 210 (?)
	chain_command_block = 211 (?)
	frosted_ice = 212 (?)
	structure_void = 217 (?)
	cobblestone_wall = 139 (wall only)



						 MinecraftElements.Block.white_glazed_terracotta = 235
						 MinecraftElements.Block.orange_glazed_terracotta = 236
						 MinecraftElements.Block.magenta_glazed_terracotta = 237
						 MinecraftElements.Block.light_blue_glazed_terracotta = 238
						 MinecraftElements.Block.yellow_glazed_terracotta = 239
						 MinecraftElements.Block.lime_glazed_terracotta = 240
						 MinecraftElements.Block.pink_glazed_terracotta = 241
						 MinecraftElements.Block.gray_glazed_terracotta = 242
						 MinecraftElements.Block.light_gray_glazed_terracotta = 243
						 MinecraftElements.Block.cyan_glazed_terracotta = 244
						 MinecraftElements.Block.purple_glazed_terracotta = 245
						 MinecraftElements.Block.blue_glazed_terracotta = 246
						 MinecraftElements.Block.brown_glazed_terracotta = 247
						 MinecraftElements.Block.green_glazed_terracotta = 248
						 MinecraftElements.Block.red_glazed_terracotta = 249
						 MinecraftElements.Block.black_glazed_terracotta = 250
						 MinecraftElements.Block.concrete = 251
						 MinecraftElements.Block.concrete_powder = 252
						 MinecraftElements.Block.structure_block = 255
#	structure_block = 235
"""

def processDoors(blockExtractor):
	"""
	Go through the blocks in blockExtractor, pulling out instances of doors.
	Once door blocks are extracted, merge the upper and lower properties
	"""

	doors = { block.location: block for block in blockExtractor.blocks if block.type in DOOR_TYPES }

	for location, door in doors.items():
		# Is the door a lower or upper door?
		if door.properties['half'] == MinecraftElements.Half.lower:
			# Try to get the upper half of the door
			upper_door = doors.get((location[0], location[1] + 1, location[2]), None)
			if upper_door is not None:
				door.properties['hinge'] = upper_door.properties['hinge']
				door.properties['powered'] = upper_door.properties['powered']
		# Upper door
		else:
			# Try to get the lower half of the door
			lower_door = doors.get((location[0], location[1] - 1, location[2]), None)
			if lower_door is not None:
				door.properties['facing'] = lower_door.properties['facing']
				door.properties['open'] = lower_door.properties['open']



def processConnection(blockTypes, connectionList):
	"""
	Functor to generate connections for the blockTypes
	"""

	def processBlock(blockExtractor):
		"""
		"""

		blocks = { block.location: block for block in blockExtractor.blocks if block.type in blockTypes }
		connections = [ tuple(block.location) for block in blockExtractor.blocks if block.type in connectionList ]

		for location, block in blocks.items():

			# For convenience
			x,y,z = location

			# Should the north connection be set?
			if (x,y,z-1) in connections:
				block.properties["north"] = True
			# Should the north connection be set?
			if (x,y,z+1) in connections:
				block.properties["south"] = True
			# Should the north connection be set?
			if (x+1,y,z) in connections:
				block.properties["east"] = True
			# Should the north connection be set?
			if(x-1,y,z) in connections:
				block.properties["west"] = True

	return processBlock



def processBars(blockExtractor):
	"""
	"""

	# Process the two sets of block types that will result extensions
	processConnection([MinecraftElements.Block.iron_bars], 
		              set.union(CONNECTION_TYPES, GLASS_AND_BAR_CONNECTION_TYPES))(blockExtractor)


	# Snow layers are a special case, if the number of layers is 8
	blocks = { tuple(block.location): block for block in blockExtractor.blocks if block.type == MinecraftElements.Block.iron_bars }
	connections = [ tuple(block.location) for block in blockExtractor.blocks if block.type == MinecraftElements.Block.snow_layer and block.properties["layers"] == 8]

	for location, block in blocks.items():

		x,y,z = location

		# Should the north connection be set?
		if (x,y,z-1) in connections:
			block.properties["north"] = True
		# Should the north connection be set?
		if (x,y,z+1) in connections:
			block.properties["south"] = True
		# Should the north connection be set?
		if(x+1,y,z) in connections:
			block.properties["east"] = True
		# Should the north connection be set?
		if(x-1,y,z) in connections:
			block.properties["west"] = True


	# Stairs require knowledge of the facing of the stairs
	connections = { tuple(block.location): block for block in blockExtractor.blocks if block.type in STAIR_TYPES }

	for location, block in blocks.items():

		x,y,z = location

		# Should a connection to the north be made?
		if (x,y,z-1) in connections.keys() and connections[(x,y,z-1)].properties["facing"] == MinecraftElements.Facing.SOUTH:
			block.properties["north"] = True
		# Should a connection to the north be made?
		if (x,y,z+1) in connections.keys() and connections[(x,y,z+1)].properties["facing"] == MinecraftElements.Facing.NORTH:
			block.properties["south"] = True
		# Should a connection to the west be made?
		if (x-1,y,z) in connections.keys() and connections[(x-1,y,z)].properties["facing"] == MinecraftElements.Facing.EAST:
			block.properties["west"] = True
		# Should a connection to the east be made?
		if (x+1,y,z) in connections.keys() and connections[(x+1,y,z)].properties["facing"] == MinecraftElements.Facing.WEST:
			block.properties["east"] = True


def processGlassPanes(blockExtractor):
	"""
	"""

	# Process the two sets of block types that will result extensions
	processConnection([MinecraftElements.Block.glass_pane, MinecraftElements.Block.stained_glass_pane], 
		              set.union(CONNECTION_TYPES, GLASS_AND_BAR_CONNECTION_TYPES))(blockExtractor)	


	# Snow layers are a special case, if the number of layers is 8
	blocks = { block.location: block for block in blockExtractor.blocks if block.type in [MinecraftElements.Block.glass_pane, MinecraftElements.Block.stained_glass_pane] }
	connections = [ tuple(block.location) for block in blockExtractor.blocks if block.type == MinecraftElements.Block.snow_layer and block.properties["layers"] == 8]

	for location, block in blocks.items():

		x,y,z = location

		# Should the north connection be set?
		if (x,y,z-1) in connections:
			block.properties["north"] = True
		# Should the north connection be set?
		if (x,y,z+1) in connections:
			block.properties["south"] = True
		# Should the north connection be set?
		if(x+1,y,z) in connections:
			block.properties["east"] = True
		# Should the north connection be set?
		if(x-1,y,z) in connections:
			block.properties["west"] = True


	# Stairs require knowledge of the facing of the stairs
	connections = { tuple(block.location): block for block in blockExtractor.blocks if block.type in STAIR_TYPES }

	for location, block in blocks.items():

		x,y,z = location

		# Should a connection to the north be made?
		if (x,y,z-1) in connections.keys() and connections[(x,y,z-1)].properties["facing"] == MinecraftElements.Facing.SOUTH:
			block.properties["north"] = True
		# Should a connection to the north be made?
		if (x,y,z+1) in connections.keys() and connections[(x,y,z+1)].properties["facing"] == MinecraftElements.Facing.NORTH:
			block.properties["south"] = True
		# Should a connection to the west be made?
		if (x-1,y,z) in connections.keys() and connections[(x-1,y,z)].properties["facing"] == MinecraftElements.Facing.EAST:
			block.properties["west"] = True
		# Should a connection to the east be made?
		if (x+1,y,z) in connections.keys() and connections[(x+1,y,z)].properties["facing"] == MinecraftElements.Facing.WEST:
			block.properties["east"] = True



def processFences(blockExtractor):
	"""
	"""

	# Process the two sets of block types that will result extensions
	processConnection(FENCE_TYPES, set.union(CONNECTION_TYPES, set([MinecraftElements.Block.cobblestone_wall])))(blockExtractor)	

	# If there is a fence of the same type in the neighboring cells,
	# set the connection
	fences = { tuple(block.location): block for block in blockExtractor.blocks if block.type in FENCE_TYPES }

	for location, fence in fences.items():

		x,y,z = location

		# Should the north connection be set?
		if (x,y,z-1) in fences and fences[(x,y,z-1)].type == fence.type:
			fence.properties["north"] = True 
		# Should the north connection be set?
		if (x,y,z+1) in fences and fences[(x,y,z+1)].type == fence.type:
			fence.properties["south"] = True 
		# Should the north connection be set?
		if (x+1,y,z) in fences and fences[(x+1,y,z)].type == fence.type:
			fence.properties["east"] = True
		# Should the north connection be set?
		if(x-1,y,z) in fences and fences[(x-1,y,z)].type == fence.type:
			fence.properties["west"] = True


def processWalls(blockExtractor):
	"""
	"""

	# Process the two sets of block types that will result extensions
	processConnection([MinecraftElements.Block.cobblestone_wall], set.union(CONNECTION_TYPES, FENCE_TYPES, GATE_TYPES, set([MinecraftElements.Block.cobblestone_wall])))(blockExtractor)	



def processStairs(blockExtractor):
	"""
	"""

	# Only need to modify stairs if other stairs are around
	stairs = { tuple(block.location): block for block in blockExtractor.blocks if block.type in STAIR_TYPES }

	for location, stair in stairs.items():
		x,y,z = location
		# What is the facing of the stair?
		if stair.properties["facing"] == MinecraftElements.Facing.NORTH:
			if (x,y,z+1) in stairs.keys() and stairs[(x,y,z+1)].properties["facing"] == MinecraftElements.Facing.EAST:
				stair.properties["shape"] = MinecraftElements.Shape.inner_right
			if (x,y,z+1) in stairs.keys() and stairs[(x,y,z+1)].properties["facing"] == MinecraftElements.Facing.WEST:
				stair.properties["shape"] = MinecraftElements.Shape.inner_left
			if (x,y,z-1) in stairs.keys() and stairs[(x,y,z-1)].properties["facing"] == MinecraftElements.Facing.EAST:
				stair.properties["shape"] = MinecraftElements.Shape.outer_right
			if (x,y,z-1) in stairs.keys() and stairs[(x,y,z-1)].properties["facing"] == MinecraftElements.Facing.WEST:
				stair.properties["shape"] = MinecraftElements.Shape.outer_left
		if stair.properties["facing"] == MinecraftElements.Facing.SOUTH:
			if (x,y,z-1) in stairs.keys() and stairs[(x,y,z-1)].properties["facing"] == MinecraftElements.Facing.EAST:
				stair.properties["shape"] = MinecraftElements.Shape.inner_left
			if (x,y,z-1) in stairs.keys() and stairs[(x,y,z-1)].properties["facing"] == MinecraftElements.Facing.WEST:
				stair.properties["shape"] = MinecraftElements.Shape.inner_right
			if (x,y,z+1) in stairs.keys() and stairs[(x,y,z+1)].properties["facing"] == MinecraftElements.Facing.EAST:
				stair.properties["shape"] = MinecraftElements.Shape.outer_left
			if (x,y,z+1) in stairs.keys() and stairs[(x,y,z+1)].properties["facing"] == MinecraftElements.Facing.WEST:
				stair.properties["shape"] = MinecraftElements.Shape.outer_right			
		if stair.properties["facing"] == MinecraftElements.Facing.EAST:
			if (x-1,y,z) in stairs.keys() and stairs[(x-1,y,z)].properties["facing"] == MinecraftElements.Facing.SOUTH:
				stair.properties["shape"] = MinecraftElements.Shape.inner_right
			if (x-1,y,z) in stairs.keys() and stairs[(x-1,y,z)].properties["facing"] == MinecraftElements.Facing.NORTH:
				stair.properties["shape"] = MinecraftElements.Shape.inner_left
			if (x+1,y,z) in stairs.keys() and stairs[(x+1,y,z)].properties["facing"] == MinecraftElements.Facing.SOUTH:
				stair.properties["shape"] = MinecraftElements.Shape.outer_right
			if (x+1,y,z) in stairs.keys() and stairs[(x+1,y,z)].properties["facing"] == MinecraftElements.Facing.NORTH:
				stair.properties["shape"] = MinecraftElements.Shape.outer_left
		if stair.properties["facing"] == MinecraftElements.Facing.WEST:
			if (x+1,y,z) in stairs.keys() and stairs[(x+1,y,z)].properties["facing"] == MinecraftElements.Facing.NORTH:
				stair.properties["shape"] = MinecraftElements.Shape.inner_right
			if (x+1,y,z) in stairs.keys() and stairs[(x+1,y,z)].properties["facing"] == MinecraftElements.Facing.SOUTH:
				stair.properties["shape"] = MinecraftElements.Shape.inner_left
			if (x-1,y,z) in stairs.keys() and stairs[(x-1,y,z)].properties["facing"] == MinecraftElements.Facing.NORTH:
				stair.properties["shape"] = MinecraftElements.Shape.outer_right
			if (x-1,y,z) in stairs.keys() and stairs[(x-1,y,z)].properties["facing"] == MinecraftElements.Facing.SOUTH:
				stair.properties["shape"] = MinecraftElements.Shape.outer_left
